const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const env = require('./env');

const Clients = require('./clients/client.model');
const Abon = require('./clients/abon.model');
const Users = require('./clients/user.model');


mongoose.connect(`mongodb://${env.user}:${env.password}@ds259253.mlab.com:59253/crossfitrivne`,{ useNewUrlParser: true })
    .then(console.log('connected !!'))
    .catch(e => console.log(e));

// server.set('view engine', 'pug');
// server.use('/css', express.static(__dirname + "/css"));
// server.use('/img', express.static(__dirname + "/img"));
// server.use('/fonts', express.static(__dirname + "/fonts"));
app.use('/js', express.static(__dirname + "/js"));




app.get("/", (req, res) => {
    res.sendFile(__dirname +'/index.html');

})


app.use(bodyParser.json())

app.listen(5000, () => console.log('running!!!!!!!!!!'));

//===================== CLIENTS CRUD ================================

//------------------------- ADD CLIENT ------------------------------

app.post('/add-client', (req, res) => {
    const user = req.body;

    Clients.find({ id: user.id })
        .then(clients => {
            console.log(clients);
            if (clients.length >= 1) {
                console.log('we are here');
                return res.status(409).json({
                    message: 'Client exists !'
                })

            } else {
                const clients = new Clients(user);
                clients.id = user.id;
                clients.fio = user.fio;
                clients.tel = user.tel;
                clients.gender = user.gender;
                clients.email = user.email;
                clients.birthday = user.birthday;
                clients.isChild = user.isChild;
                clients.debtor = false ;
                clients.abon_name = user.abon_name;
                clients.abon_cap = user.abon_cap;
                clients.first_reg_date = user.first_reg_date;
                clients.buy_abon_date = user.buy_abon_date;
                clients.trainings_amount = user.trainings_amount;
                clients.trainings_sum = user.trainings_sum;
                clients.last_visit_date = user.last_visit_date;
                clients.receptionist = user.receptionist;
                clients.discount = user.discount;
                clients.notes = user.notes;
                clients.buy_sum = user.buy_sum;
                clients.frost = user.frost;
                clients.photo = user.photo;
                clients.save();
                res.status(200).send({message:"CLIENT IS ADDED"})
            }
        })
});

//--------------------------- FIND CLIENT -------------------------------

app.get('/find-client', (req, res) => {
    const data = req.body;
 
if(data.id){
    Clients.find({ id:data.id })
        .then(users => {
                res.send(users);
        })
}else if(data.tel){
    Clients.find({ tel:data.tel })
    .then(users => {
            res.send(users);
    })
}
});

//--------------------------- FIND CLIENTS -------------------------------

app.get('/find-clients', (req, res) => {
  
    Clients.find()
        .then(clients => {
                res.send(clients);
        })
});

//-------------------------- UPDATE CLIENT ---------------------------------

app.put('/update-client', (req, res) => {
const user = req.body;

    Clients.updateOne({ id:user.id },
    {$set:{
        fio:user.fio,
        tel:user.tel
    }})
        .then(users => {
                res.send(users);
        })
});
 //------------------------------ Delete Client ---------------------------------
 app.delete('/delete-client', (req, res) => {
    const user = req.body;
        Clients.find({ id:user.id }).remove()
            .then(() => { res.send({message:"CLIENT REMOVED"})})
            .catch(e => console.log(e))
    })

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

//========================== USERS CRUD ==========================================

//-------------------------- Create User -----------------------------------------

app.post('/add-user', (req, res) => {
    const user = req.body;

    Users.find({ id: user.id })
        .then(users => {
            console.log(users);
            if (users.length >= 1) {
                console.log('we are here');
                return res.status(409).json({
                    message: 'User exists !'
                })

            } else {
                const _user = new Users(user);
                _user.id = user.id;
                _user.fio = user.fio;
                _user.tel = user.tel;
                _user.birthday = user.birthday;
                _user.role = user.role;
                _user.on_service = user.on_service;
                _user.on_service_time_date = user.on_service_time_date;
                _user.off_service_time_date = user.off_service_time_date;
                _user.photo = user.photo;
                _user.save();
                res.status(200).send({message:"USER IS ADDED"})
            }
        })
});

//------------------------- Find User -----------------------------------

app.get('/find-user', (req, res) => {
    const data = req.body;
    // console.log(data);
if(data.id){
    Users.findOne({ id:data.id })
        .then(user => {
                res.send(user);
        })
}else if(data.tel){
    Users.findOne({ tel:data.tel })
    .then(user => {
            res.send(user);
    })
}
});

//------------------------- Find Users -----------------------------------

app.get('/find-users', (req, res) => {
    Users.find()
        .then(user => {
                res.send(user);
        })
});

//-------------------------- Update User --------------------------

app.put('/update-user', (req, res) => {
    
const user = req.body;

    Clients.updateOne({ id:user.id },
    {$set:{
        on_service:user.on_service,
        on_service_time_date:user.on_service_time_date,
        off_service_time_date:user.off_service_time_date
    }})
        .then(users => {
                res.send(users);
        })

});


//------------------------- Delete User --------------------------------

app.delete('/delete-user', (req, res) => {
    const data = req.body;

        Users.find({ id:data.id }).remove()
            .then(() => { res.send({message:"USER REMOVED"}) })
            .catch(e => console.log(e))
    });


//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

//========================== ABONS CRUD ==========================================

//-------------------------- Create Abon -----------------------------------------

app.post('/add-abon', (req, res) => {
    const abon = req.body;

    Abon.find({ id: abon.id })
        .then(abons => {
            console.log(abons);
            if (abons.length >= 1) {
                console.log('we are here');
                return res.status(409).json({
                    message: 'Abon exists !'
                })

            } else {
                const _abon = new Abon(abon);
                _abon.id = abon.id;
                _abon.name = abon.name;
                _abon.amount = abon.amount;
                _abon.price = abon.price;
                _abon.save();
                res.status(200).send({message:"ABON IS ADDED"})
            }
        })
});

//------------------------- Find Abon -----------------------------------

app.get('/find-abon', (req, res) => {
    const data = req.body;

    Abon.find()
        .then(abons => {
                res.send(abons);
});
});

//------------------------- Update Abon -----------------------------------

app.put('/update-abon', (req, res) => {
const data = req.body;

    Abon.updateOne({ id:data.id },
    {$set:{
        name:data.name,
        amount:data.amount,
        price:data.price
    }})
        .then(abons => {
                res.send(abons);
        })

});

//------------------------- Delete Abon --------------------------------

app.delete('/delete-abon', (req, res) => {

const data = req.body;

        Abon.find({ id:data.id }).remove()
            .then(() => { res.send({message:"ABON REMOVED"}) })
            .catch(e => console.log(e))
    });

//||||||||||||||||||||||| SERVER RUNNING! ||||||||||||||||||||||||||||||

