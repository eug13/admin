const mongoose = require('mongoose');
const Schema = mongoose.Schema;

SchemaAbon = new Schema({
    id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    amount: {
        type: String,
        required: true
    },
    price: {
        type: String,
        required: true
    }
})


module.exports = mongoose.model("abon", SchemaAbon);