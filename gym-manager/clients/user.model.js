const mongoose = require('mongoose');
const Schema = mongoose.Schema;

SchemaUsers = new Schema({
    id: {
        type: String,
        required: true
    },
    fio: {
        type: String,
        required: true
    },
    tel: {
        type: String,
        required: true
    },
    birthday: {
        type: String,
        required: false
    },
    role: {
        type: String,
        required: false// default receptionist
    },
    on_service: {
        type: Boolean,
        required: false
    },
    on_service_time_date: {
        type: String,
        required: false
    },
    off_service_time_date: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: false
    }
})


module.exports = mongoose.model("users", SchemaUsers);