const mongoose = require('mongoose');
const Schema = mongoose.Schema;

SchemaClient = new Schema({
    id: {
        type: String,
        required: true
    },
    fio: {
        type: String,
        required: true
    },
    tel: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false
    },
    birthday: {
        type: String,
        required: false
    },
    isChild: {
        type: Boolean,
        required: false
    },
    debtor: {
        type: Boolean,
        required: false
    },
    abon_name: {
        type: String,
        required: false
    },
    abon_cap: {
        type: String,
        required: false
    },
    first_reg_date: {
        type: String,
        required: false
    },
    buy_abon_date: {
        type: String,
        required: false
    },
    trainings_amount: {
        type: String,
        required: false
    },
    trainings_sum: {
        type: String,
        required: false
    },
    last_visit_date: {
        type: String,
        required: false
    },
    receptionist: {
        type: String,
        required: false
    },
    discount: {
        type: String,
        required: false
    },
    notes: {
        type: String,
        required: false
    },
    buy_sum: {
        type: String,
        required: false
    },
    frost: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: false
    }
})


module.exports = mongoose.model("clients", SchemaClient);