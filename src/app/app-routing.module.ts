import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { DetailsComponent } from './details/details.component';
import { PostsComponent } from './posts/posts.component';
import { CommentsComponent }from './comments/comments.component';
import { HelloComponent } from './hello/hello.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { ReactiveFormsComponent } from './reactive-forms/reactive-forms.component'

const routes: Routes = [
  {
    path:'',
    component:HelloComponent
  },
  {
    path:'users',
    component:UsersComponent
  },
  {
    path:'details/:id',
    component:DetailsComponent
  },
  {
    path:'posts',
    component:PostsComponent
  },
  {
    path:'posts/:id/comments',
    component:CommentsComponent
  },
  {
    path:'contact-form',
    component:ContactFormComponent
  },
  {
    path:'reactive-form',
    component:ReactiveFormsComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
