import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.scss']
})
export class ReactiveFormsComponent implements OnInit {

  rForm:FormGroup;
  post:any;
  description:string = '';
  name:string = '';
  titleAlert:string = 'This.is required!';
  descAlert:string = 'You must specify a description that\'s between 30 and 500 characters.';

  constructor(private fb:FormBuilder){

    this.rForm = fb.group({
      'name' : [null, Validators.required],
      'description' : [null, Validators.compose([Validators.required, Validators.minLength(20), Validators.maxLength(500)])],
      'validate' : ''
    });
  }

  addPost(post){
    this.name = post.name;
    this.description = post.description;
  }

  ngOnInit(){
    this.rForm.get('validate').valueChanges.subscribe(
      (validate)=>{
        if(validate == '1'){
          this.rForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
          this.titleAlert = "You need to specify at least 3 characters.";
        } else {
          this.rForm.get('name').setValidators(Validators.required);
        }
        this.rForm.get('name').updateValueAndValidity();
      }
    )
  }

}
