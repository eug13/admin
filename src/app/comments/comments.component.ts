import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from "@angular/router";
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
  animations: [
    trigger('listStagger', [
      transition('* <=> *', [
        query(':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger('50ms', animate('550ms ease-out',
              style({ opacity: 1, transform: 'translateY(0px)' })))
          ],{ optional:true }),
          query(':leave', animate('50ms ease-out',style({ opacity: 0 })),{ optional:true })
      ])
    ])
  ]
})

export class CommentsComponent implements OnInit {
  
  params$:string;
  comments$:Object;

  constructor(private route: ActivatedRoute, private data: DataService) { 
    this.route.params.subscribe(params => this.params$ = params.id );
  }

  ngOnInit() {
    this.data.getComments(this.params$).subscribe(
      data => this.comments$ = data
    )
  }

}
