import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss']
})
export class HelloComponent implements OnInit {
  today$:Object;

  constructor() {  }

  ngOnInit() {
    let date = new Date().toDateString();
    this.today$ = date;
  }

}
