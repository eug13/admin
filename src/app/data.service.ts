import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }

  getUsers(){
    return this.http.get('https://jsonplaceholder.typicode.com/users')
  }

  getUser(userId){
    return this.http.get('https://jsonplaceholder.typicode.com/users/'+userId)
  }

  getPosts(){
    return this.http.get('https://jsonplaceholder.typicode.com/posts')
  }
  getComments(postId){
    let Id = postId;
    if(!postId)Id=1;
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${Id}/comments`)
  }
  // getComment(postId){
  //   console.log(postId)
  //   let Id = postId;
  //   if(!postId)Id=1;
  //   return this.http.get(`https://jsonplaceholder.typicode.com/posts/${Id}/comments`)
  // }

  

}
