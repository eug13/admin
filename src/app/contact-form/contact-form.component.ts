import { Component } from '@angular/core';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent {
  // ADD CLIENTS FORM
  id$:string;
  fio$:string;
  tel$:string;
  gender$:string;
  bday$:string;
  child$:string;
  abon$:string;
  discount$:string;
  notes$:string;
  photo$:string;
  abons:Object[] = [{id: 1,val:"one"},{id:2,val:"two"}];




  handleID(x) { 
     this.id$ = x;
     console.log(this.id$) 
  }

  handleFIO(x) { 
    this.fio$ = x;
    console.log(this.fio$) 
 }

 handleTEL(x) { 
  this.tel$ = x;
  console.log(this.tel$) 
}

handleGENDER(x) { 
  this.gender$ = x;
  console.log(this.gender$) 
}

handleBDAY(x) { 
  this.bday$ = x;
  console.log(this.bday$) 
}

handleCHILD(x) { 
  this.child$ = x;
  console.log(this.child$) 
}

handleABON(x) { 
  this.abon$ = x;
  console.log(this.abon$) 
}

handleDISCOUNT(x) { 
  this.discount$ = x;
  console.log(this.discount$) 
}

handleNOTES(x) { 
  this.notes$ = x;
  console.log(this.notes$) 
}

handlePHOTO(x) { 
  this.photo$ = x;
  console.log(this.photo$) 
}

handleCLIENT(){


  let client = {
    id:this.id$,
    fio:this.fio$,
    tel:this.tel$,
    gender:this.gender$,
    birthday:this.bday$,
    isChild:this.child$,
    debtor:false,
    abon_name:this.abon$,
    first_reg_date:new Date().toLocaleDateString(),
    buy_abon_date:new Date().toLocaleDateString(),
    trainings_ammount:null,
    trainings_sum:null,//duplicated
    last_visit_date:new Date().toLocaleDateString(),
    receptionist:null,
    discount:this.discount$,
    notes:this.notes$,
    buy_sum:null,//duplicated
    frost:null,
    photo:this.photo$
  }
  console.log(client,"------->")
}

}
